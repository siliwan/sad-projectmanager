<?php

use Slim\Http\Request;
use Slim\Http\Response;
use \Interop\Container\ContainerInterface as ContainerInterface;

$container = $app->getContainer();

//Include globals
require('Controllers/phtmlHeader.php');

$settings = $app->get('settings', '');

// Routes

$app->get('/', 'src\Controllers\homeController:index')->setName('p.home');

$app->get('/settings', 'src\Controllers\settingsController:index')->setName('p.settings');;

$app->group('/projects', function () {

    $this->get('', 'src\Controllers\projectController:index')->setName("p.projects.index");

    $this->get('/', 'src\Controllers\projectController:index')->setName("p.projects.index");

    $this->get('/{pr_name}', 'src\Controllers\projectController:projects')->setName("p.projects.project");
});
