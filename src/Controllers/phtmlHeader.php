<?php

namespace src\Controllers;

class phtmlHeader {

    function getHeader($args = array()) {

        $public_dir = '/';

        $header_response = '    <meta charset="utf-8"/>
                                <meta http-equiv="cache-control" content="max-age=0" />
                                <title>' . (array_key_exists('title', $args) ? $args['title'] : 'ProjectManager') .'</title>
                                <link href="//fonts.googleapis.com/css?family=Lato:300" rel="stylesheet" type="text/css">
                                <link rel="stylesheet" type="text/css" href="' . $public_dir . 'css/default.css">
                                <link href="http://maxcdn.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet">
                                '. (!array_key_exists('extCSS', $args) ? '' : phtmlHeader::getExtCSS($args));
        echo $header_response;
    }

    function getExtCSS($args) {

        $cssRegex = '/^.*\.(css)$/i'; 

        if(is_array($args)){

            foreach ($args['extCSS'] as $item) {
                if(preg_match($cssRegex, $item)) {
                    return '<link rel="stylesheet" type="text/css" href="' . $public_dir . 'css/'. $item .'">';
                } else {
                    return '<link rel="stylesheet" type="text/css" href="' . $public_dir . 'css/'. $item .'.css">';
                }
            }
        } else {
            if(preg_match($cssRegex, $args)) {
                return '<link rel="stylesheet" type="text/css" href="' .  $public_dir . 'css/'. $item .'">';
            } else {
                return '<link rel="stylesheet" type="text/css" href="' .  $public_dir . 'css/'. $item .'.css">';
            }
        }
    }
}