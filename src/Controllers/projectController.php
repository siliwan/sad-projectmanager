<?php

namespace src\Controllers;

use \Datetime;
use \DateInterval;
use Noodlehaus\Config;

class projectController {   

    protected $container;

    // constructor receives container instance
    public function __construct(\Slim\Container $container) {
        $this->container = $container;
    }

    //If no project was selected, this shall appear

    public function index($request, $response, $args) {
        $result = array();
        if(isset($_GET['create'])) {
          $args['get'] = $_GET;
        } 
        else if(isset($_GET['prName']) && $_GET['prName'] != null) {
          array_push($result, $this->createProject($_GET));
        }

        $this->container->logger->info("Slim-Skeleton '/projects' route");

        $settings = $this->container->get('settings', '');
        $args['gSettings'] = $settings['general'];
        $args['isProjectList'] = true;
        $args['result'] = $result;

        //getAllProjects()
        $args['projectConfig'] = $this->getConfig();

        return $this->container->view->render($response, 'projects.html', $args);
    }

    //If a project was selected, this shall appear

    public function projects($request, $response, $args) {
      $this->container->logger->info("Slim-Skeleton '/projects/" . $args['pr_name'] . "' route");

      $settings = $this->container->get('settings', '');
      $args['gSettings'] = $settings['general'];
      $args['isProjectList'] = false;

      $args['projectConfig'] = $this->getConfig();

      //echo array_search($args['pr_name'], $args['projectConfig']);
      //echo "Result: " . array_search($args['pr_name'], $args['projectConfig']);
      $args['currentProject'] = $this->searchArrayForID($args['pr_name'], $args['projectConfig']['knownProjects']['projects']);
      $args['currentProjectPath'] = ROOT . $settings['general']['projectFolder'] . '\\' . $args['currentProject']['project']->name;

      //getAllProjects()
      return $this->container->view->render($response, 'projects.html', $args);
    }

    /*
    public function input($request, $response, $args) {
      $response->getbody()->write("Method: POST");
      return;
    }
    */

    private function createProject($get) {
       //Get settings array
       $settings = $this->container['settings'];

       //Init folder in which all projects lay with the settings variable
       $projectFolder = ROOT . $settings['general']['projectFolder'];
 
       //Setup variables that will be set later but used by outerfunctions
       $projectScan = null;
       $projectFolderCount = null;
       $config = null;

       //Setup variable for unique ID
       $uniqid = uniqid(true);

       //Check if all neccessery parameters were given to us (custom color will always be default = )
       if(!isset($get['prName']) && !isset($get['prDescr'])) {
        return array(
          "exception" => "true",
          "reason" => "Not all required parameters were given (Needed: Name + Description)"
        );
       }

       $projectScan = array();

       //Put any folder found into a seperate array created above
       foreach(glob($projectFolder. "/*", GLOB_ONLYDIR) as $dir) {
        array_push($projectScan,$dir);
       }

       //Loop through as long as you will get a name which does not match any existing folders
       if($this->compareNameArray($get, glob($projectFolder. "/*", GLOB_ONLYDIR))) {
        return array(
          "exception" => "true",
          "reason" => "This name already exists"
        );
       }

       //create directory to put in project OR die if it can't be created for any reason
      if (!file_exists($projectFolder. $get['prName']) && !mkdir($projectFolder . $get['prName'], 0777, true)) {
        return array(
          "exception" => "true",
          "reason" => "FATAL: Project folder could not be created. Does it already exist or does the application not have enough permissions?"
        ); 
      } else {
        $file = fopen($projectFolder . $get['prName'] . "/.projm", "w") or die("Unable to open/create file!");
        $txt = "John Doe\n";
        fwrite($file, $txt);
        fclose($file);
      }

    }

    /*
     * Function compareNameArray
     * @param Array with get request to analyze
     * @param String value to compare
     * 
     * @return true or false wethever the comparison was successful or not
     *
    */
    private function compareNameArray($key, $name) {
      foreach($key as $dir) {
        if($dir == $name) {
          return true;
        } else {
          return false;
        }
       }
    }

    private function getConfig() {
      //Get settings array
      $settings = $this->container['settings'];

      //Init folder in which all projects lay with the settings variable
      $projectFolder = ROOT . $settings['general']['projectFolder'];

      //Setup variables that will be set later but used by outerfunctions
      $projectScan = null;
      $projectFolderCount = null;
      $config = null;

      //Setup variable for time comparison
      //$dateTime = new DateTime(date("Y-m-d\TH:i:sP"));
      //$dateTime60 = $dateTime->add(new DateInterval('PT30M'));
      $settingsTimeData = new DateTime($settings['knownProjects']['lastUpdated']);
      $currentDatTime = date('m/d/Y h:i:s a', time());
      
      //--- REMOVE ---\\

      //echo "Current time: " . $settingsTimeData->format("Y-m-d\TH:i:sP");

      //$projectFile = $projectFolder . '/sampleProject/.projm';
      //$projectFile = readfile($projectFile, 0);
      //$projectSubFolders = count( glob($projectFolder, GLOB_ONLYDIR) );
      //echo 'ProjectCount-value before: ' . $settings['knownProjects']['projectCount'];
      
      //--- ENDREMOVE --- \\

      //Create Array in which all projects will be stored
      $projects = array();


      //If projectlist is older than 60 mins, it shall be updated or if the update button has been issued
      //if($settingsTimeData->diff(new DateTime)->format('%R') == '+60') {
      if(true) {
        //True:
          //Fetch the sub-folders in the project folder, then add one by one to an array with the .projm file
          $projectScan = array();

          foreach(glob($projectFolder. "/*", GLOB_ONLYDIR) as $dir) {
            array_push($projectScan, $dir);
          }
          $projectFolderCount = (count($projectScan));

          for ($i=0; $i < $projectFolderCount; $i++) { 
            $currentFileDataRaw = file_get_contents($projectScan[$i] . '/.projm');
            array_push($projects, json_decode($currentFileDataRaw));
          }

          if(!is_file($projectFolder . '/.projects.json')) {
            $currentFile = fopen($projectFolder . '/.projects.json', 'w');
            //fwrite($currentFile,  "<?php \n //This file was automatically generated and updated. Do not touch anything unless you know what you are doing \n return array('knownProjects' => array('projectCount' => ". $projectFolderCount . ",'projects' => array()), 'lastUpdated' => \"" . $currentDatTime . "\");");
            fwrite($currentFile,  '{'. PHP_EOL . '"knownProjects" : {'. PHP_EOL . '  "projectCount" : "",'. PHP_EOL . '  "projects" : {},'. PHP_EOL . '  "lastUpdated" : "' . $currentDatTime . '"'. PHP_EOL . ' }'. PHP_EOL . '}');
            fclose($currentFile);
            $this->container->logger->info("Generated project config");
          }
          
          //Update settings list

          //$settings['knownProjects']['projectCount'] = $projectFolderCount;

          //file_put_contents(ROOT . "erc/settings.php", "");

          $configRaw = file_get_contents($projectFolder . '/.projects.json');
          $config = json_decode($configRaw, true);
          //var_dump($config);

          //echo $config['knownProjects']['projectCount'];
          //echo "<br>";
          $config['knownProjects']['projectCount'] = $projectFolderCount;

          $config['knownProjects']['projects'] = array();

          foreach ($projects as $project) {
            array_push($config['knownProjects']['projects'], (array)$project);
          }

          file_put_contents($projectFolder . '/.projects.json', json_encode($config));

          $this->container->logger->info("Updated project list. " . $projectFolderCount . " project(s) were found");
          
      } else {
        //False:

          //get Array from settings
      }

      /*
      if($config['knownProjects']['projectCount'] > 0 && $settings['general']['isFirstStart'] == true) {
        $settings['general']['isFirstStart'] = false;
      }
      */

      //return array
      return $config;
      
    }

    private function searchArrayForID($arg, $array) {
      foreach ($array as $key) {
       //echo "Projectname: " . $key['project']->name. ', ID: ' . $key['project']->id . '<br>';
       if($key['project']->id == $arg) {
         return $key;
       }
      }
    }
}