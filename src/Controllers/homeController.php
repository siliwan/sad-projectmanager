<?php

namespace src\Controllers;

class homeController {   

    protected $container;

    // constructor receives container instance
    public function __construct(\Slim\Container $container) {
        $this->container = $container;
    }

    public function index($request, $response, $args) {
        $this->container->logger->info("Slim-Skeleton '/' route");

        $settings = $this->container->get('settings', '');
        $args['gSettings'] = $settings['general'];
        return $this->container->view->render($response, 'home.html', $args);
    }
}