<?php

class Breadcrumb{

    const HOME_NAME     = 'Start';
    const TAG_PARENT_O     = '<div class="ui breadcrumb">';
    const TAG_PARENT_C     = '</div>';
    const TAG_DIVIDER     = '<span class="divider">/</span>';
    const CLASS_ACTIVE  = 'active';

    const BREAD = [
        'account'    => [
            'title'        => 'Account',
            'c'            => [
                'settings'    => [
                    'title'        => 'Settings',
                ],
                'info'    => [
                    'title'        => 'Account-Info',
                ],
            ]
        ],
        'admin'        => [
            'title'        => 'Administration',
            'c'            => [
                'actions'    => [
                    'title'        => 'Actions'
                ],
                'config' => [
                    'title'        => 'Configuration'
                ],
                'users'        => [
                    'title'        => 'Users'
                ],
            ]
        ],
        'tos'    => [
            'title'        => 'Terms of Service'
        ],
    ];

    private static function trimGetParams($str){
        if (strpos($str, '?') !== false) {
            return substr($str, 0, strpos($str, '?'));
        } else {
            return $str;
        }
    }

    public static function generate(){
        $u = explode('/', $_SERVER['REQUEST_URI']);
        $b = [[
            'url'    => '/',
            'title'    => self::HOME_NAME,
        ]];
        $p = self::BREAD;
        foreach ($u as $key => $value) {
            if (empty($value)) continue;
            if (array_key_exists($value, $p)) {
                $b[] = [
                    'url'     => $value,
                    'title' => self::trimGetParams($p[$value]['title'])
                ];
                if (array_key_exists('c', $p[$value])) $p = $p[$value]['c'];
            } else {
                $b[] = [
                    'url'     => $value,
                    'title' => self::trimGetParams(ucfirst(strtolower($value)))
                ];
            }
        }
        $f = self::TAG_PARENT_O;
        $u = '';
        foreach ($b as $key => $value) {
            $ake = array_key_exists($key + 1, $b);
            $u .= (($key > 1) ? '/': '').$value['url'];
            $tag = 'a';
            $class = 'section';
            if (!$ake) $tag = 'div'; $class .= ' '.self::CLASS_ACTIVE;
            $f .= '<'.$tag.' href="'.$u.'" class="'.$class.'">'.$value['title'].'</'.$tag.'>';
            if ($ake) $f .= self::TAG_DIVIDER;
        }
        $f .= self::TAG_PARENT_C;
        return $f;
    }
}
