<?php 

$baseControllersDir = '/Controllers';

//Additional classes
//require_once __DIR__ . $baseControllersDir . '/phtmlHeader.php';
require __DIR__ . $baseControllersDir . '/breadcrumb.php';

//Route logic
require __DIR__ . $baseControllersDir . '/homeController.php';
require __DIR__ . $baseControllersDir . '/settingsController.php';
require __DIR__ . $baseControllersDir . '/projectController.php';