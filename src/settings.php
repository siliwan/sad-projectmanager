<?php
return [
    'settings' => [
        'debug' => true,
        'displayErrorDetails' => true, // set to false in production
        'addContentLengthHeader' => false, // Allow the web server to send the content-length header

        // Renderer settings
        'renderer' => [
            'template_path' => __DIR__ . '/../templates/',
        ],

        // Monolog settings
        'logger' => [
            'name' => 'slim-app',
            'path' => isset($_ENV['docker']) ? 'php://stdout' : __DIR__ . '/../logs/app.log',
            'level' => \Monolog\Logger::DEBUG,
        ],

        'general' => [
            'app-name' => 'pm-projectmanager',
            'app-version' => '0.2.0',
            'app-title' => 'ProjectManager',
            'isFirstStart' => 'true',
            'projectFolder' => 'projects',
        ],

    ],
];
