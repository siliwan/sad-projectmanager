<?php
// DIC configuration

$container = $app->getContainer();

// view renderer
$container['renderer'] = function ($c) {
    $settings = $c->get('settings')['renderer'];
    return new Slim\Views\PhpRenderer($settings['template_path']);
};

$container['errorHandler'] = function ($c) {
    return function ($request, $response, $exception) use ($container) {
        //Format of exception to return
        $data = [
            'message' => $exception->getMessage()
        ];
        return $container->get('response')->withStatus($response->getStatus())
            ->withHeader('Content-Type', 'application/json')
            ->write(json_encode($data));
    };
};

// monolog
$container['logger'] = function ($c) {
    $settings = $c->get('settings')['logger'];
    $logger = new Monolog\Logger($settings['name']);
    $logger->pushProcessor(new Monolog\Processor\UidProcessor());
    $logger->pushHandler(new Monolog\Handler\StreamHandler($settings['path'], $settings['level']));
    return $logger;
};

$container['view'] = function ($container) {
    $view = new \Slim\Views\Twig('../templates', [
        'debug' => true,
        //'cache' => 'cache'
    ]);

    // Instantiate and add Slim specific extension
    $basePath = rtrim(str_ireplace('index.php', '', $container['request']->getUri()->getBasePath()), '/');
    $view->addExtension(new Slim\Views\TwigExtension($container['router'], $basePath));
    $view->addExtension(new Twig_Extension_Debug());
    $view->addExtension(new Twig_Extensions_Extension_Text());

    return $view;
};

//Add filters for Twig

$filters[] = $filter = new Twig_SimpleFilter('dumping', function ($dumpArgs) {
                return var_dump($dumpArgs);
            });

if(isset($filters)) {
    foreach ($filters as $filter) {
        $container->get('view')->getEnvironment()->addFilter($filter);
    }
}


//Class specific things

class CustomTwig extends \Slim\Views\Twig {
	public function render(Psr\Http\Message\ResponseInterface $response, $template, $data = []) {
		$data = array_merge($data, [
			'breadcrumb'	=> Breadcrumb::generate(),
		]);
		$response->getBody()->write($this->fetch($template, $data));
		return $response;
	}
}